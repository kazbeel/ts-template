const baseConfig = require('./jest.base.config');

/**
 * @type { import('jest'.Config) }
 */
module.exports = {
  ...baseConfig,
  displayName: 'E2e Tests',
  testMatch: ['<rootDir>/test/**/*.e2e.spec.ts'],
};
