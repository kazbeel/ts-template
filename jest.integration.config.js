const baseConfig = require('./jest.base.config');

/**
 * @type { import('jest'.Config) }
 */
module.exports = {
  ...baseConfig,
  displayName: 'Integration Tests',
  testMatch: ['<rootDir>/test/**/*.integration.spec.ts'],
};
