const baseConfig = require('./jest.base.config');

/**
 * @type { import('jest'.Config) }
 */
module.exports = {
  ...baseConfig,
  displayName: 'Unit Tests',
  testMatch: ['<rootDir>/test/**/*.spec.ts'],
  testPathIgnorePatterns: [
    '<rootDir>/test/.*\\.integration\\.spec\\.ts$',
    '<rootDir>/test/.*\\.e2e\\.spec\\.ts$',
  ],
};
