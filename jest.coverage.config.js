const baseConfig = require('./jest.base.config');

/**
 * @type { import('jest'.Config) }
 */
module.exports = {
  ...baseConfig,
  displayName: 'Coverage',
  collectCoverage: true,
  coverageDirectory: '<rootDir>/coverage',
  collectCoverageFrom: ['<rootDir>/src/**/*.{ts,js}'],
  coverageReporters: process.env.CI ? ['lcov', 'cobertura'] : ['lcov', 'text'],
};
