import express, { Request, Response } from 'express';
import 'dotenv/config';

export const app = express();

// Health check endpoint
app.get('/health', (_req: Request, res: Response) => {
  res.sendStatus(200);
});

// API endpoint
app.get('/api', (_req: Request, res: Response) => {
  res.status(200).json({ message: 'Hello API!' });
});

const PORT = 3000;
export const server = app.listen(PORT, () => {
  // eslint-disable-next-line no-console
  console.log(`Server is running on port ${PORT}`);
});
