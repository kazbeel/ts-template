# ts-template

Template for setting up a TypeScript service following modern best practices.

## Table of Contents

- [Features](#features)
- [Setup](#setup)
- [Scripts](#scripts)
- [Testing](#testing)
- [Docker Usage](#docker-usage)
- [Configuration](#configuration)
- [Contributing](#contributing)

## Features

- **Code Quality**: Uses ESLint, Prettier, and an `.editorconfig` file to maintain consistency.
- **Testing**: Supports modularized Jest setups for unit, integration, e2e, and coverage testing.
- **Containerization**: Dockerfile with multi-stage builds and Compose support for development and production.
- **Git Hooks**: Pre-commit and commit message linting with Husky and Lint-Staged.

## Setup

1. Clone the repository:

   ```bash
   git clone https://gitlab.com/kazbeel/ts-template.git
   cd ts-template
   ```

2. Install dependencies:

   ```bash
   npm install
   ```

3. Configure environment variables by copying `.env.example` to `.env` and updating values as necessary.

## Scripts

Available npm scripts:

- `npm run build`: Builds the application with SWC for production.
- `npm run build:dev`: Builds the application for development.
- `npm start`: Runs the production build.
- `npm run start:dev`: Starts the app in development mode with live reloading.
- `npm run lint`: Lints TypeScript files.
- `npm run lint:fix`: Fixes linting issues.
- `npm run format`: Formats code with Prettier.
- `npm test`: Runs unit tests.

For a full list of commands, see the `scripts` section in `package.json`.

## Testing

Jest configurations:

- **Unit Tests**: `jest.unit.config.js`
- **Integration Tests**: `jest.integration.config.js`
- **End-to-End Tests**: `jest.e2e.config.js`
- **Coverage**: `jest.coverage.config.js`

To run specific tests:

```bash
# Run unit tests
npm run test:unit

# Run integration tests
npm run test:integration

# Run end-to-end tests
npm run test:e2e

# Run tests with coverage
npm run test:coverage
```

## Docker Usage

### Build and Run

#### Development

Build and start the development container:

````
```bash
docker-compose up app
````

#### Production

Build and start the production container:

````
```bash
docker-compose up app_prod
````

### Dockerfile Stages

- Base: Installs dependencies for both environments.
- Development: Sets up a development environment with live reloading.
- Build: Compiles TypeScript to JavaScript and prunes dev dependencies.
- Production: Runs the compiled code with minimal footprint.

## Configuration

### SWC

- `swc.dev.js`: Development configuration (with comments, source maps).
- `swc.prod.js`: Production configuration (no comments, minified).

### Commit Standards

Uses `husky` for git hooks, `commitlint` for commit message formatting, and `lint-staged` to ensure code quality pre-commit.

## Contributing

1. Fork the repository.
1. Create a feature branch.
1. Open a Pull Request for review.

## License

This project is licensed under the MIT License.
