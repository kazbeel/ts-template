# Base Stage
# ---------------------------------------------------------
FROM node:20.18-slim AS base

ENV PATH /app/node_modules/.bin:$PATH

WORKDIR /app
RUN chown node:node /app
USER node


# Development Stage
# ---------------------------------------------------------
FROM base AS development

ENV NODE_ENV=development

COPY --chown=node:node package.json package-lock.json ./

RUN npm ci \
  && npm cache clean --force

WORKDIR /app/source

CMD [ "npm", "run", "start:dev" ]


# Build Stage
# ---------------------------------------------------------
FROM base AS build

COPY --chown=node:node package.json package-lock.json ./

RUN npm ci

COPY --chown=node:node . ./

RUN npm run build \
  && npm prune --production \
  && npm cache clean --force


# Production Stage
# ---------------------------------------------------------
FROM base AS production

ENV NODE_ENV=production

COPY --chown=node:node --from=build /app/dist/* ./
COPY --chown=node:node --from=build /app/node_modules ./node_modules/

CMD [ "node", "index.js" ]
