/**
 * @type { import('jest'.Config) }
 */
module.exports = {
  verbose: true,
  testEnvironment: 'node',
  transform: {
    '^.+\\.tsx?$': ['@swc/jest', { sourceMaps: 'inline' }],
  },
  roots: ['<rootDir>/test'],
  extensionsToTreatAsEsm: ['.ts'],
};
