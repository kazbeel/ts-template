import request from 'supertest';
import { app, server } from '../src/index';

describe('Express Service', () => {
  afterAll(async () => {
    await new Promise((res) => {
      server?.close(res);
    });
  });

  it('returns 200 on the health check endpoint', async () => {
    const response = await request(app).get('/health');

    expect(response.status).toBe(200);
  });

  it('returns 200 and JSON response on the /api endpoint', async () => {
    const response = await request(app).get('/api');

    expect(response.status).toBe(200);
    expect(response.body).toEqual({ message: 'Hello API!' });
  });
});
